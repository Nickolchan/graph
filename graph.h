#include <iostream>
#include <vector>
#include <unordered_map>
#include "stdafx.h"
#include <string>
#include <fstream>
#include <sstream>
#include <set>
#include <algorithm>
#include <stack>
using namespace std;

class PairHasher
{
public:
    long long operator()(const pair<int, int> &p) const
    {
        return p.first * 100000ll + p.second;
    }
};

struct Edge
{
    int from;
    int to;
    int next;
    int flow;
    int cap;

    Edge() {}

    Edge(int from, int to, int next, int f, int c) : from(from), to(to), next(next), flow(f), cap(c)
    {}
};

class Graph
{
public:
    enum Type { ADJ_MATRIX, ADJ_LIST, EDGES_LIST };

    Graph()
    {
        size = 0;
        type = ADJ_LIST;
        isOriented = isWeighted = false;
    }

    Graph(int n, Type type = ADJ_LIST, bool isOriented = false, bool isWeighted = false)
    {
        this->size = n;
        this->type = type;
        this->isOriented = isOriented;
        this->isWeighted = isWeighted;
        switch(this->type)
        {
        case ADJ_MATRIX:
            adjMatrix.assign(n + 1, vector<int>(n + 1));
            break;
        case ADJ_LIST:
            adjList.assign(n + 1, unordered_map<int, int>());
            break;
        case EDGES_LIST:
            break;
        }
    }

    void readGraph(string fileName);

    void writeGraph(string fileName);

    void transformToAdjMatrix();

    void transformToAdjList();

    void transformToEdgesList();

    void addEdge(int from, int to, int weight = 1);

    void removeEdge(int from, int to);

    int changeEdge(int from, int to, int newWeight);

    Graph getSpanningTreePrima();

    Graph getSpanningTreeKruskal();

    Graph getSpanningTreeBoruvka();

    int checkEuler(bool &circleExist);

    int countEdges();

    vector<int> getEuleranTourFleri();

    vector<int> getEuleranTourEffective();

    bool dfsWithMarks(int u, int part, vector<int>& marks);

    bool checkBipart(vector<int> &marks);

    bool try_kuhn(int u, vector<bool> &used, vector<int> &mt);

    vector<pair<int, int> > getMaximumMatchingBipart();

    int dfsPush(int u, int sink, int flow, vector<Edge> &edges, vector<int> &head, vector<bool> &used) const;

    Graph flowFordFulkerson(int source, int sink);

    Graph flowDinic(int source, int sink);

    int getSize() const;

private:
    Type type;
    int size;
    bool isOriented;
    bool isWeighted;
    vector<vector<int> > adjMatrix;
    vector < unordered_map<int, int> > adjList;
    unordered_map<pair<int, int>, int, PairHasher> edgesList;

    int countNonEmptyComponents();
    bool existWay(int u, int destination, pair<int, int> lockedEdge, vector<bool> &used);
    Graph copy();


};

class DSU
{
private:
    vector<int> p;
public:
    DSU(int N);
    int count();
    int find(int x);
    void unite(int x, int y);
};

DSU::DSU(int N)
{
    for(int i = 0; i < N; i++)
    {
        p.push_back(i);
    }
}

int DSU::find(int x)
{
    if (x == p[x])
        return x;
    return find (p[x]);
}

void DSU::unite(int x, int y)
{
    x = find(x);
    y = find(y);
    if (x != y)
        p[y] = x;
}

int DSU::count()
{
    return p.size();
}

int Graph::getSize() const
{
    return size;
}

/*
 * Лабораторная №1
 */

void Graph::readGraph(string fileName)
{
    ifstream fin;
    fin.open(fileName, ifstream::in);
    char symbol;
    fin >> symbol;
    switch (symbol)
    {
    case 'C':
        this->type = ADJ_MATRIX;
        fin >> this->size >> this->isOriented >> this->isWeighted;
        this->adjMatrix.assign(this->size + 1, vector<int>(this->size + 1, 0));
        for (int i = 1; i <= this->size; ++i)
            for (int j = 1; j <= this->size; ++j)
                fin >> this->adjMatrix[i][j];
        break;
    case 'L':
        this->type = ADJ_LIST;
        fin >> this->size >> this->isOriented >> this->isWeighted;
        this->adjList.resize(this->size + 1);
        fin.ignore();
        for (int i = 1; i <= this->size; ++i)
        {
            string line;
            getline(fin, line);
            stringstream ss(line);
            int b, w;
            if (this->isWeighted)
                while (ss >> b >> w)
                    this->adjList[i][b] = w;
            else while (ss >> b)
                this->adjList[i][b] = 1;
        }
        break;
    case 'E':
        this->type = EDGES_LIST;
        int numOfEdges;
        fin >> this->size >> numOfEdges >> this->isOriented >> this->isWeighted;
        for (int i = 0; i < numOfEdges; ++i)
        {
            int from, to, w = 1;
            fin >> from >> to;
            if (this->isWeighted)
                fin >> w;
            this->edgesList[make_pair(from, to)] = w;
        }
        break;
    }
    fin.close();
}

void Graph::writeGraph(string fileName)
{
    ofstream fout(fileName, ofstream::out);
    switch (type)
    {
    case ADJ_MATRIX:
        fout << "C " << size << endl;
        fout << (isOriented ? 1 : 0) << ' ' << (isWeighted ? 1 : 0) << endl;
        for (int i = 1; i <= size; ++i)
        {
            for (int j = 1; j <= size; ++j)
                fout << adjMatrix[i][j] << ' ';
            fout << endl;
        }
        break;
    case ADJ_LIST:
        fout << "L " << size << endl;
        fout << (isOriented ? 1 : 0) << ' ' << (isWeighted ? 1 : 0) << endl;
        for (int i = 1; i <= size; ++i)
        {
            for (auto p : adjList[i])
            {
                fout << p.first << ' ';
                if (isWeighted)
                    fout << p.second << ' ';
            }
            fout << endl;
        }
        break;
    case EDGES_LIST:
        fout << "E " << size << ' ' << edgesList.size() << endl;
        fout << (isOriented ? 1 : 0) << ' ' << (isWeighted ? 1 : 0) << endl;
        for (auto edge : edgesList)
        {
            fout << edge.first.first << ' ' << edge.first.second;
            if (isWeighted)
                fout << ' ' << edge.second;
            fout << endl;
        }
        break;
    }
    fout.close();
}

void Graph::transformToAdjMatrix()
{
    switch (type)
    {
    case ADJ_LIST:
        adjMatrix.assign(size + 1, vector<int>(size + 1, 0));
        for (int i = 1; i <= size; ++i)
            for (auto p : adjList[i])
                adjMatrix[i][p.first] = p.second;
        adjList.clear();
        break;
    case EDGES_LIST:
        adjMatrix.assign(size + 1, vector<int>(size + 1, 0));
        for (auto edge : edgesList)
        {
            adjMatrix[edge.first.first][edge.first.second] = edge.second;
            if (!isOriented)
                adjMatrix[edge.first.second][edge.first.first] = edge.second;
        }
        edgesList.clear();
        break;
    }
    type = ADJ_MATRIX;
}

void Graph::transformToAdjList()
{
    switch (type)
    {
    case ADJ_MATRIX:
        adjList.assign(size + 1, unordered_map<int, int>());
        for (int i = 1; i <= size; ++i)
            for (int j = 1; j <= size; ++j)
                if (adjMatrix[i][j])
                    adjList[i][j] = adjMatrix[i][j];
        adjMatrix.clear();
        break;
    case EDGES_LIST:
        adjList.assign(size + 1, unordered_map<int, int>());
        for (auto edge : edgesList)
        {
            adjList[edge.first.first][edge.first.second] = edge.second;
            if (!isOriented)
                adjList[edge.first.second][edge.first.first] = edge.second;
        }
        break;
    }
    type = ADJ_LIST;
}

void Graph::transformToEdgesList()
{
    switch (type)
    {
    case ADJ_MATRIX:
        for (int i = 1; i <= size; ++i)
            for (int j = 1; j <= size; ++j)
                if (adjMatrix[i][j] && (i <= j || isOriented))
                    edgesList[make_pair(i, j)] = adjMatrix[i][j];
        adjMatrix.clear();
        break;
    case ADJ_LIST:
        for (int i = 1; i <= size; ++i)
            for (auto p : adjList[i])
                if (i <= p.first || isOriented)
                    edgesList[make_pair(i, p.first)] = p.second;
        adjList.clear();
        break;
    }
    type = EDGES_LIST;

}

void Graph::addEdge(int from, int to, int weight)
{
    switch (type)
    {
    case ADJ_MATRIX:
        adjMatrix[from][to] = weight;
        if (!isOriented)
            adjMatrix[to][from] = weight;
        break;
    case ADJ_LIST:
        adjList[from][to] = weight;
        if (!isOriented)
            adjList[to][from] = weight;
        break;
    case EDGES_LIST:
        edgesList[make_pair(from, to)] = weight;
        break;
    }
}

void Graph::removeEdge(int from, int to)
{
    switch (type)
    {
    case ADJ_MATRIX:
        adjMatrix[from][to] = 0;
        if (!isOriented)
            adjMatrix[to][from] = 0;
        break;
    case ADJ_LIST:
        adjList[from].erase(to);
        if (!isOriented)
            adjList[to].erase(from);
        break;
    case EDGES_LIST:
        edgesList.erase(make_pair(from, to));
        if(!isOriented)
            edgesList.erase(make_pair(to, from));
        break;
    }
}

int Graph::changeEdge(int from, int to, int newWeight)
{
    int oldWeight = 0;
    switch (type)
    {
    case ADJ_MATRIX:
        oldWeight = adjMatrix[from][to];
        adjMatrix[from][to] = newWeight;
        if (!isOriented)
            adjMatrix[to][from] = newWeight;
        break;
    case ADJ_LIST:
        oldWeight = adjList[from][to];
        adjList[from][to] = newWeight;
        if (!isOriented)
            adjList[to][from] = newWeight;
        break;
    case EDGES_LIST:
        if(isOriented)
        {
            oldWeight = edgesList[make_pair(from, to)];
            edgesList[make_pair(from, to)] = newWeight;
        }
        else
        {
            if(edgesList.find(make_pair(from, to))!=edgesList.end())
            {
                oldWeight = edgesList[make_pair(from, to)];
                edgesList[make_pair(from, to)] = newWeight;
            }
            else
            {
                oldWeight = edgesList[make_pair(to, from)];
                edgesList[make_pair(to, from)] = newWeight;
            }
        }
        break;
    }
    return oldWeight;
}

/*
 * Лабораторная №2
 */

Graph Graph::getSpanningTreePrima()
{
    transformToAdjList();
    vector<int> dist(size + 1, 1e9);
    vector<int> from(size + 1, -1);
    vector<int> used(size + 1, false);

    dist[1] = 0;
    set<pair<int, int> > q;
    q.insert(make_pair(0, 1));
    while (q.size() > 0)
    {
        int u = q.begin()->second;
        used[u] = true;
        q.erase(q.begin());
        for(auto p : adjList[u])
        {
            if (!used[p.first] && p.second < dist[p.first])
            {
                q.erase(make_pair(dist[p.first], p.first));
                dist[p.first] = p.second;
                from[p.first] = u;
                q.insert(make_pair(dist[p.first], p.first));
            }
        }
    }

    Graph spanTree(size, ADJ_LIST, false, isWeighted);
    for (int i = 2; i <= size; ++i)
        spanTree.addEdge(from[i], i, adjList[from[i]][i]);
    return spanTree;
}

Graph Graph::getSpanningTreeKruskal()
{
    DSU dsu(size + 1);
    transformToEdgesList();
    vector<pair<int, pair<int, int> > > edges;
    for(auto edge : edgesList)
        edges.push_back(make_pair(edge.second, edge.first));
    sort(edges.begin(), edges.end());
    Graph spanTree(size, ADJ_LIST, false, true);
    for(auto e : edges)
    {
        int u = e.second.first;
        int v = e.second.second;
        if (dsu.find(u) != dsu.find(v))
        {
            dsu.unite(u, v);
            spanTree.addEdge(e.second.first, e.second.second, e.first);
        }
    }
    return spanTree;

}

Graph Graph::getSpanningTreeBoruvka()
{
    transformToAdjList();
    DSU dsu(size + 1);
    Graph spanTree(size, EDGES_LIST, false, true);
    while (spanTree.edgesList.size() < size - 1)
    {

        unordered_map<int, pair<int, pair<int, int> > > minEdge;

        for (int i = 1; i <= size; ++i)
            minEdge[dsu.find(i)] = make_pair(1e9, make_pair(-1, -1));

        for (int i = 1; i <= size; ++i)
        {
            int root = dsu.find(i);
            for(auto e : adjList[i])
                if (dsu.find(e.first) != root)
                {
                    minEdge[root] = min(minEdge[root], make_pair(e.second, make_pair(i, e.first)));
                }
        }

        for each(pair<int, pair<int, pair<int, int> > > e in minEdge)
        {
            int weight = e.second.first;
            int from = e.second.second.first;
            int to = e.second.second.second;
            if (spanTree.edgesList.find(make_pair(to, from)) == spanTree.edgesList.end())
            {
                spanTree.addEdge(from, to, weight);
                dsu.unite(from, to);
            }
        }
    }
    return spanTree;
}

/*
 * Лабораторная №3
 */

int Graph::countNonEmptyComponents()
{
    transformToEdgesList();
    DSU dsu(size + 1);
    for(auto e : edgesList)
        dsu.unite(e.first.first, e.first.second);
    set<int> roots;
    for (int i = 1; i <= size; ++i)
        if (dsu.find(i) != i)
            roots.insert(dsu.find(i));
    return roots.size();
}

int Graph::checkEuler(bool& circleExist)
{
    if (countNonEmptyComponents()>1)
    {
        circleExist = false;
        return 0;
    }
    transformToAdjList();
    int oddCount = 0;
    for (int i = 1; i <= size; ++i)
        if (adjList[i].size() % 2 == 1)
            oddCount++;
    if (oddCount == 0)
    {
        circleExist = true;
        return 1;
    }
    circleExist = false;
    if (oddCount == 2)
    {
        for (int i = 1; i <= size; ++i)
            if (adjList[i].size() % 2 == 1)
                return i;
    }
    else return 0;
}

int Graph::countEdges()
{
    transformToEdgesList();
    return edgesList.size();
}

bool Graph::existWay(int u, int destination, pair<int, int> lockedEdge, vector<bool> &used)
{
    if (u == destination)
        return true;
    used[u] = true;
    for(auto e : adjList[u])
        if (!used[e.first] && make_pair(u, e.first) != lockedEdge)
            if (existWay(e.first, destination, lockedEdge, used))
                return true;
    return false;
}

Graph Graph::copy()
{
    transformToEdgesList();
    Graph graphCopy(size, EDGES_LIST, isOriented, isWeighted);
    graphCopy.edgesList = edgesList;
    return graphCopy;
}

vector<int> Graph::getEuleranTourFleri()
{
    int numOfEdges = countEdges();
    transformToAdjList();
    auto graphCopy = this->copy();
    bool circleExist;
    int start = checkEuler(circleExist);
    vector<int> tour;
    tour.push_back(start);
    if (start)
    {
        int u = start;
        for (int j = 0; j < numOfEdges; ++j)
        {
            vector<int> bridges, non_bridges;

            for (auto e : adjList[u])
            {
                vector<bool> used(size + 1, false);
                if (existWay(u, e.first, make_pair(u, e.first), used))
                    non_bridges.push_back(e.first);
                else bridges.push_back(e.first);
            }

            if (non_bridges.size()>0)
            {
                tour.push_back(non_bridges[0]);
                removeEdge(u, non_bridges[0]);
                u = non_bridges[0];
            }
            else
            {
                tour.push_back(bridges[0]);
                removeEdge(u, bridges[0]);
                u = bridges[0];
            }
        }
    }
    *this = graphCopy.copy();
    return tour;

}

vector<int> Graph::getEuleranTourEffective()
{
    transformToAdjList();
    bool circleExist;
    int start = checkEuler(circleExist);
    vector<int> tour;
    if (start)
    {
        unordered_map<pair<int, int>, bool, PairHasher> used;
        stack<int> st;
        st.push(start);
        while (!st.empty())
        {
            int u = st.top();

            for (auto e : adjList[u])
            {
                int v = e.first;
                if (!used[make_pair(u, v)] && !used[make_pair(v, u)])
                {
                    st.push(v);
                    used[make_pair(u, v)] = true;
                    break;
                }
            }
            if (u == st.top())
            {
                st.pop();
                tour.push_back(u);
            }
        }
    }
    return tour;
}

/*
 * Лабораторная №4
 */

bool Graph::dfsWithMarks(int u, int part, vector<int>& marks)
{
    if (marks[u] != -1)
        return marks[u] == part;
    marks[u] = part;
    bool isBipart = true;
    for (auto e : adjList[u])
        isBipart &= dfsWithMarks(e.first, 1 - part, marks);
    return isBipart;
}

bool Graph::checkBipart(vector<int>& marks)
{
    transformToAdjList();
    marks.assign(size + 1, -1);
    bool isBipart = true;
    for (int i = 1; i <= size; ++i)
        if (marks[i] == -1)
            isBipart &= dfsWithMarks(i, 0, marks);
    return isBipart;
}

bool Graph::try_kuhn(int u, vector<bool>& used, vector<int>& mt)
{
    if (used[u]) return false;
    used[u] = true;
    for(auto e : adjList[u])
    {
        int to = e.first;
        if(mt[to]==-1 || try_kuhn(mt[to], used, mt))
        {
            mt[to] = u;
            return true;
        }
    }
    return false;
}

vector<pair<int, int>> Graph::getMaximumMatchingBipart()
{
    vector<pair<int, int> > matching;
    vector<int> marks(size + 1, -1);
    vector<bool> used(size + 1, false);
    vector<int> mt(size + 1, -1);
    if(checkBipart(marks))
    {
        transformToAdjList();

        vector<bool> preused(size + 1, false);
        for (int i = 1; i <= size; ++i)
            if(marks[i]==0)
                for(auto e : adjList[i])
                    if(mt[e.first]==-1)
                    {
                        mt[e.first] = i;
                        preused[i] = true;
                        break;
                    }

        for (int i = 1; i <= size; ++i)
            if(marks[i]==0 && !preused[i])
            {
                used.assign(size + 1, false);
                try_kuhn(i, used, mt);
            }
        for (int i = 1; i <= size; ++i)
            if (mt[i] != -1)
                matching.push_back(make_pair(mt[i], i));
    }
    return matching;
}

int Graph::dfsPush(int u, int sink, int flow, vector<Edge>& edges, vector<int>& head, vector<bool>& used) const
{
    if (u == sink) return flow;
    used[u] = true;
    for (int i = head[u]; i != -1; i=edges[i].next)
        if(!used[edges[i].to] && edges[i].flow<edges[i].cap)
        {
            int pushed = dfsPush(edges[i].to, sink, min(flow, edges[i].cap - edges[i].flow), edges, head, used);
            if(pushed)
            {
                edges[i].flow += pushed;
                edges[i ^ 1].flow -= pushed;
                return pushed;
            }
        }
    return 0;
}


